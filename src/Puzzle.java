// Kasutatud materjalid:
// https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
// https://stackoverflow.com/questions/127704/algorithm-to-return-all-combinations-of-k-elements-from-n

import java.util.ArrayList;
import java.util.List;

public class Puzzle {

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {

      String[] test = new String[]{"YKS", "KAKS", "KOLM"}; // 234 solutions
      String[] test2 = new String[]{"SEND", "MORE", "MONEY"}; // 1 solution
      String[] test3 = (new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"});

      String[] arr = {"1","2","3","4","5","6","7","8","9","0"};
      combinations2(arr, letters(test).size(), 0, new String[letters(test).size()], test);
      System.out.println(countCount());

   }
   public static int count = 0;

   public static String countCount() {
      if (count == 0) {
         return "There are no solutions";
      }
      return "Possible solutions: " + count;
   }

   public static List<String> letters (String[] original) {  // leiame kõik esinevad tähed, duplikaadid välja
      List<String> letter_list = new ArrayList<String>();
      for (int i = 0; i < original.length; i++) {
         for (int j = 0; j < original[i].length(); j++) {
            if (!letter_list.contains(original[i].substring(j, j+1))) {
               letter_list.add(original[i].substring(j, j+1));
            }
         }
      }
      return letter_list;
   }

   public static void permute(String str, int l, int r, String[] original) {
      List<String> letter_list = letters(original);

      if (l == r) {
         //System.out.println(str);
         List<String> created_values = new ArrayList<String>();
         for (int x = 0; x < letter_list.size(); x++) {  // määra igale tähele number
            created_values.add(letter_list.get(x) + "=" + str.charAt(x));
         }

         String first_word = new String();
         for (int z = 0; z < original[0].length(); z++) {
            for (int q = 0; q < created_values.size(); q++) {
               if (original[0].substring(z, z + 1).equals(created_values.get(q).substring(0, 1))) {
                  first_word = first_word + created_values.get(q).substring(2, 3);
               }
            }
         }

         String second_word = new String();
         for (int rx = 0; rx < original[1].length(); rx++) {
            for (int s = 0; s < created_values.size(); s++) {
               if (original[1].substring(rx, rx + 1).equals(created_values.get(s).substring(0, 1))) {
                  second_word = second_word + created_values.get(s).substring(2, 3);
               }
            }
         }

         String answer = new String();
         for (int p = 0; p < original[2].length(); p++) {
            for (int yz = 0; yz < created_values.size(); yz++) {
               if (original[2].substring(p, p + 1).equals(created_values.get(yz).substring(0, 1))) {
                  answer = answer + created_values.get(yz).substring(2, 3);
               }
            }
         }

         if (Long.parseLong(first_word) + Long.parseLong(second_word) == Long.parseLong(answer) && (!first_word.substring(0,1).equals("0") && !second_word.substring(0,1).equals("0"))) {
            System.out.println(created_values);
            count++;
         }

      } else {
         for (int i = l; i <= r; i++)
         {
            str = swap(str,l,i);
            permute(str, l+1, r, original);
            str = swap(str,l,i);
         }
      }
   }

   /**
    * Swap Characters at position
    * @param a string value
    * @param i position 1
    * @param j position 2
    * @return swapped string
    */
   public static String swap(String a, int i, int j) {
      char temp;
      char[] charArray = a.toCharArray();
      temp = charArray[i] ;
      charArray[i] = charArray[j];
      charArray[j] = temp;
      return String.valueOf(charArray);
   }

   static void combinations2(String[] arr, int len, int startPosition, String[] result, String[] original){
      if (len == 0){
         String str = new String();

         for (int x = 0; x < result.length; x++) {
            str = str + result[x]; }
         int n = str.length();
         permute(str, 0, n-1, original);
         return;
      }
      for (int i = startPosition; i <= arr.length-len; i++){
         result[result.length - len] = arr[i];
         combinations2(arr, len-1, i+1, result, original);
      }
   }
}


